#!/usr/bin/env python

# Plot on-shell HH observables and their uncertainty bands

import readyoda
import yodarun
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rcParams['lines.linewidth'] = 1.0

xlabels = {
'HH_dPhi' : r'$\Delta\phi$(H, H)   (GeV)',
'HH_dR' : r'$\Delta R$(H, H)',
'HH_deta' : r'$\Delta\eta$(H, H)',
'HH_mass' : r'HH mass   (GeV)',
'HH_pT' : r'HH $p_{\mathrm{T}}$   (GeV)',
'H_eta' : r'HH $\eta$',
'H_eta1' : r'$\mathrm{H}_1 \eta$',
'H_eta2' : r'$\mathrm{H}_2 \eta$',
'H_jet1_dR' : r'',
'H_jet1_deta' : r'',
'H_pT' : r'H $p_{\mathrm{T}}$   (GeV)',
'H_pT1' : r'',
'H_pT2' : r'',
'H_phi' : r'',
'jet_HT' : r'',
'jet_eta_1' : r'',
'jet_eta_2' : r'',
'jet_eta_3' : r'',
'jet_eta_4' : r'',
'jet_mass_1' : r'',
'jet_mass_2' : r'',
'jet_mass_3' : r'',
'jet_mass_4' : r'',
'jet_multi_exclusive' : r'Exclusive jet multiplicity',
'jet_multi_inclusive' : r'Inclusive jet multiplicity',
'jet_pT_1' : r'1. jet $p_{\mathrm{T}}$   (GeV)',
'jet_pT_2' : r'2. jet $p_{\mathrm{T}}$   (GeV)',
'jet_pT_3' : r'3. jet $p_{\mathrm{T}}$   (GeV)',
'jet_pT_4' : r'4. jet $p_{\mathrm{T}}$   (GeV)',
'jet_y_1' : r'1. jet rapidity',
'jet_y_2' : r'2. jet rapidity',
'jet_y_3' : r'3. jet rapidity',
'jet_y_4' : r'4. jet rapidity',
'jets_dR_12' : r'',
'jets_dR_13' : r'',
'jets_dR_23' : r'',
'jets_deta_12' : r'',
'jets_deta_13' : r'',
'jets_deta_23' : r'',
'jets_dphi_12' : r'',
'jets_dphi_13' : r'',
'jets_dphi_23' : r'',
'N' : 'Number of events (?)',
'pmN' : 'Number of positive/negative events (?)',
'pmXS' : 'Positive/negative generated cross section (?)',
}

logplot = {
'HH_dPhi' : False,
'HH_dR' : False,
'HH_deta' : False,
'HH_mass' : True,
'HH_pT' : True,
'H_eta' : False,
'H_eta1' : False,
'H_eta2' : False,
'H_jet1_dR' : False,
'H_jet1_deta' : False,
'H_pT' : True,
'H_pT1' : False,
'H_pT2' : False,
'H_phi' : False,
'jet_HT' : False,
'jet_eta_1' : False,
'jet_eta_2' : False,
'jet_eta_3' : False,
'jet_eta_4' : False,
'jet_mass_1' : False,
'jet_mass_2' : False,
'jet_mass_3' : False,
'jet_mass_4' : False,
'jet_multi_exclusive' : True,
'jet_multi_inclusive' : True,
'jet_pT_1' : True,
'jet_pT_2' : True,
'jet_pT_3' : True,
'jet_pT_4' : True,
'jet_y_1' : False,
'jet_y_2' : False,
'jet_y_3' : False,
'jet_y_4' : False,
'jets_dR_12' : False,
'jets_dR_13' : False,
'jets_dR_23' : False,
'jets_deta_12' : False,
'jets_deta_13' : False,
'jets_deta_23' : False,
'jets_dphi_12' : False,
'jets_dphi_13' : False,
'jets_dphi_23' : False,
'N' : False,
'pmN' : False,
'pmXS' : False,
}


global_xs_scale_factor = 1000.

ylabel = 'Cross section (fb) / abscissa unit'



# The `runs` correspond to the different setups that were used and that will be compared in the plots
# `runs` is a dictionary of "label : run_directory", where the label will appear in the legend
def plot(title, identifier, histogram_path, runs, additionalruns=[]):
    fig, (ax0, ax1) = plt.subplots(2, sharex=True, gridspec_kw = {'height_ratios':[3, 1]}, figsize=(6.4,6.4))
    
    #print fig.get_size_inches()
    
    hatches = ['xxxxxxx', '\\\\\\\\\\\\\\', '///////', '+++']
    linestyles = ['-', '--', ':', '-.']
    
    for i, run in enumerate(runs):
        x = run.binedges(histogram_path)
        if i == 0:
            ax1.plot([x[0], x[-1]], [1., 1.], 'k-', linewidth=0.5)
            #ax2.plot([x[0], x[-1]], [1., 1.], 'k-', linewidth=0.5)
            #ax3.plot([x[0], x[-1]], [1., 1.], 'k-', linewidth=0.5)
        nominal = run.nominal(histogram_path)
        nominal_line = ax0.plot(x, nominal*global_xs_scale_factor, linestyles[i], label=run.label)
        statup, statdown = run.statistical_uncertainty_up_down(histogram_path)
        ax0.fill_between(x, statup*global_xs_scale_factor, statdown*global_xs_scale_factor, color=nominal_line[0].get_color(), alpha=0.4, linewidth=0)
        pdfup, pdfdown = run.pdf_variation_up_down(histogram_path)
        #print pdfup[10], nominal[10], pdfdown[10]
        ax0.fill_between(x, pdfup*global_xs_scale_factor, pdfdown*global_xs_scale_factor, color=nominal_line[0].get_color(), alpha=0.4, linewidth=0)
        scaleup, scaledown = run.scale_variation_up_down(histogram_path)
        #print scaleup[::2], nominal[::2], scaledown[::2]
        #print scaleup, nominal, scaledown
        
        ax0.fill_between(x, scaleup*global_xs_scale_factor, scaledown*global_xs_scale_factor, facecolor='none', hatch=hatches[i], edgecolor=nominal_line[0].get_color(), linewidth=0)

        if i < 2:
            ax1.fill_between(x, pdfup/nominal, pdfdown/nominal, color=nominal_line[0].get_color(), alpha=0.4, linewidth=0)
            ax1.fill_between(x, scaleup/nominal, scaledown/nominal, facecolor='none', hatch=hatches[i], edgecolor=nominal_line[0].get_color(), linewidth=0)
        #elif i < 3:
            #ax2.fill_between(x, pdfup/nominal, pdfdown/nominal, color=nominal_line[0].get_color(), alpha=0.4, linewidth=0)
            #ax2.fill_between(x, scaleup/nominal, scaledown/nominal, facecolor='none', hatch=hatches[i], edgecolor=nominal_line[0].get_color(), linewidth=0)
        # elif i < 4:
        #     ax3.fill_between(x, pdfup/nominal, pdfdown/nominal, color=nominal_line[0].get_color(), alpha=0.4, linewidth=0)
        #     ax3.fill_between(x, scaleup/nominal, scaledown/nominal, facecolor='none', hatch=hatches[i], edgecolor=nominal_line[0].get_color(), linewidth=0)
    
    # for run in additionalruns:
    #     if not len(runs) > 2: break
    #     reference = runs[2].nominal(histogram_path)
    #     variation = run.nominal(histogram_path) # / 0.01946772
    #     variation_line = ax2.plot(x, variation/reference, '--', label=run.label)
    #     #print reference/variation
    #     variation_line = ax0.plot(x, variation, '--', label=run.label)

    #ax0.fill_between(x[4:], high_matrixy[4:], low_matrixy[4:], color=matrixline[0].get_color(), alpha=0.4, linewidth=0)
    #totalerrorartist = ax0.fill_between(x, (1. - totaly/100.)*datay/36.07456 , (1. + totaly/100.)*datay/36.07456 , label='Total uncertainty', facecolor="none", hatch='//////', edgecolor='0.75', linewidth=0.0)
    
    fig.canvas.draw()
    #ax3.set_ylim(ymin=max(ax2.get_ylim()[0], 0.8), ymax=min(ax2.get_ylim()[1], 1.2))
    #ax2.set_ylim(ax3.get_ylim()) # equal to those of ax 3!
    #ax1.set_ylim(ax3.get_ylim()) # equal to those of ax 3!
    #ax1.set_ylim(ymin=0.8, ymax=1.2)
    #ax1.set_ylim((0.9, 1.1))
    ax1.set_ylabel('Ratio to HH @ LO')
    #ax2.set_ylabel('Rel. uncert.')
    #ax3.set_ylabel('Rel. uncert.')
    #ax0.set_title(observables.titles[identifier], loc='left')
    #ax0.set_title(r'ATLAS Internal   $\sqrt{s} = 13\;\mathrm{TeV}$, $36.1\;\mathrm{fb}^{-1}$', loc='left', weight='black')
    #ax0.set_title(r'', weight='black', x=0.02, y=0.9, ha='left', transform=ax0.transAxes, size='x-large')
    if logplot[identifier]:
        ax0.set_yscale('log', nonposy='clip')
        ax0.set_ylim(ymax=ax0.get_ylim()[1]*10.)
    else:
        ax0.set_ylim(ymin=0.0, ymax=ax0.get_ylim()[1]*1.3)
    #ax1.xaxis.labelpad = 9
    #ax0.yaxis.labelpad = 10
    #ax1.yaxis.labelpad = 10
    
    #ax0.yaxis.set_label_coords(-0.09, 1.0)
    #ax1.yaxis.set_label_coords(-0.09, 0.5)
    
    ax1.set_xlabel(xlabels[identifier], ha='right', x=1.) # , size='xx-large'
    ax0.set_ylabel(ylabel, ha='right', y=1.) # , size='xx-large'
    
    # ATLAS style
    ax0.set_xlim((min(x), max(x)))
    ax0.tick_params(which='both', bottom='on', top='on', left='on', right='on', direction='in')
    ax1.tick_params(which='both', bottom='on', top='on', left='on', right='on', direction='in')
    ax1.tick_params(axis='x', pad=5)
    # ax2.tick_params(which='both', bottom='on', top='on', left='on', right='on', direction='in')
    # ax2.tick_params(axis='x', pad=5)
    # ax3.tick_params(which='both', bottom='on', top='on', left='on', right='on', direction='in')
    # ax3.tick_params(axis='x', pad=5)
    
    ax0.legend(frameon=False)
    ax0.set_title(title, loc='left', size='medium')
    
    fig.subplots_adjust(left=0.15, right=0.95, top=0.95, bottom=0.1, hspace=0.07)    
    fig.savefig(identifier + '.pdf')










def main():
    
    #title = r'$pp\, \to\, e^+e^-\mu^+\mu^-$ with \textsc{Herwig}'
    title = r'Matrix elements: OpenLoops (full theory), MadGraph (HEFT)'
    
    # label : path to run directory
    runspecs = {
        r'$\mathrm{pp} \to \mathrm{HH}$ @ LO' : '../event_generation/thesis/HH/gghh_ol.yoda',
        r'$\mathrm{pp} \to \mathrm{HH}$ @ LO+PS' : '../event_generation/thesis/HH_PS/gghh_ol.yoda',
        r'$\mathrm{pp} \to \mathrm{HH + jet}$ @ LO' : '../event_generation/thesis/HHj/gghh_ol.yoda',
        #r'$gg \to \ell^+\ell^-\ell^{\prime +}\ell^{\prime -}$ @ LO (OpenLoops)' : '../event_generation/ggllll_ol/ggllll_ol.yoda',
        #r'$gg \to \ell^+\ell^-\ell^{\prime +}\ell^{\prime -}$ @ LO (GoSam)' : '../event_generation/ggllll_gs/ggllll_gs.yoda',
    }
    
    runs = []
    for label, nominalyoda in runspecs.iteritems():
        runs.append(yodarun.Run(label, nominalyoda))
    
    histogram_paths = readyoda.get_histogram_paths('../event_generation/thesis/HH/gghh_ol.yoda')
    histogram_names = readyoda.get_histogram_names('../event_generation/thesis/HH/gghh_ol.yoda')
    print histogram_names
    
    for hp, hn in zip(histogram_paths, histogram_names):
        print 'Plotting {0}...'.format(hn)
        plot(title, hn, hp, runs)



if __name__ == '__main__':
    main()
