# Analysis

To produce results, run the following commands:

```bash
./plot_llll.py
```

The following must be installed:

* Yoda (comes with Rivet)
* Numpy
* Matplotlib

In case of questions email stefan.richter@cern.ch
