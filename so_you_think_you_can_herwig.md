# Some hints for Herwig development

By Stefan Richter, intended for own use, but perhaps you find them useful.

## Informative output

Getting more runtime info:

```bash
read Matchbox/IncreaseVerbosity.in
set /Herwig/Generators/EventGenerator:IntermediateOutput Yes
```

## Configuring runs / setting parameters

Herwig's interactive mode is useful for snooping around in the Herwig config "repository":

```bash
Herwig run # to open prompt, then in Herwig prompt you can do stuff like:
help all
lspaths
cd Herwig/MatrixElements/Matchbox/Amplitudes
ls
describe GoSam
describe GoSam:IsLoopInduced
```

## Development with matrix elements

You can do [NLO consistency checks](https://herwig.hepforge.org/tutorials/advanced/index.html#nlo-consistency-checks).

You can set virtual corrections to zero:

```bash
set Factory:VirtualContributions No
```
