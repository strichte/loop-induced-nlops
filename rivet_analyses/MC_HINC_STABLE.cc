// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"

namespace Rivet {


  /// @brief MC validation analysis for stable-Higgs events
  class MC_HINC_STABLE : public Analysis {
  public:

    /// Default constructor
    MC_HINC_STABLE()
      : Analysis("MC_HINC_STABLE")
    {    }


    /// @name Analysis methods
    //@{

    /// Book histograms
    void init() {
      Cut cut = Cuts::abseta < 3.5 && Cuts::pT > 25*GeV;
      IdentifiedFinalState ifs(Cuts::abseta < 10.0 && Cuts::pT > 0*GeV);
      ifs.acceptId(25);
      addProjection(ifs, "IFS");
      // /// @todo Urk, abuse! Need explicit HiggsFinder and TauFinder?
      // ZFinder hfinder(FinalState(), cut, PID::TAU, 115*GeV, 135*GeV, 0.0, ZFinder::NOCLUSTER, ZFinder::NOTRACK, 125*GeV);
      // addProjection(hfinder, "Hfinder");
      _h_H_mass = bookHisto1D("H_mass", 50, 119.7, 120.3);
      _h_H_pT = bookHisto1D("H_pT", logspace(100, 1.0, 0.5*(sqrtS()>0.?sqrtS():14000.)/GeV));
      _h_H_pT_peak = bookHisto1D("H_pT_peak", 25, 0.0, 25.0);
      _h_H_y = bookHisto1D("H_y", 40, -4, 4);
      _h_H_phi = bookHisto1D("H_phi", 25, 0.0, TWOPI);
    }


    /// Do the analysis
    void analyze(const Event & e) {
      const IdentifiedFinalState& ifs = applyProjection<IdentifiedFinalState>(e, "IFS");
      Particles higgses = ifs.particlesByPt();
      if (higgses.empty()) vetoEvent;
      FourMomentum hmom = higgses[0].momentum(); // any additional Higgses are ignored, only the highest-pT one is taken into account
      
      const double weight = e.weight();

      _h_H_mass->fill(hmom.mass()/GeV, weight);
      _h_H_pT->fill(hmom.pT()/GeV, weight);
      _h_H_pT_peak->fill(hmom.pT()/GeV, weight);
      _h_H_y->fill(hmom.rapidity(), weight);
      _h_H_phi->fill(hmom.phi(), weight);
    }


    /// Finalize
    void finalize() {
      const double xsec = crossSection()/picobarn;
      normalize(_h_H_mass, xsec);
      normalize(_h_H_pT, xsec);
      normalize(_h_H_pT_peak, xsec);
      normalize(_h_H_y, xsec);
      normalize(_h_H_phi, xsec);
    }

    //@}


  private:

    /// @name Histograms
    //@{
    Histo1DPtr _h_H_mass;
    Histo1DPtr _h_H_pT;
    Histo1DPtr _h_H_pT_peak;
    Histo1DPtr _h_H_y;
    Histo1DPtr _h_H_phi;
    //@}

  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_HINC_STABLE);

}
