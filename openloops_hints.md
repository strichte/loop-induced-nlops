# Notes for OpenLoops usage

[Public process libraries](http://openloops.hepforge.org/process_library.php?repo=public)

[Beta process libraries](http://openloops.hepforge.org/process_library.php?repo=public_beta)

## Configuration file (`openloops.cfg`)

You can configure OpenLoops via the file `openloops.cfg` in its installation directory. After installation with the Herwig bootstrap script, it might look something like this:

```bash
[OpenLoops]
fortran_compiler = /opt/local/bin/gfortran-mp-4.9
# fortran_tool = 'gfortran'
compile_extra = 0
```

To be able to install processes from both the `public` and `public_beta` repository, add:

```bash
process_repositories = public,public_beta
```
